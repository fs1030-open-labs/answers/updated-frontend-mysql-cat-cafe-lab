import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AllCats from "../src/CatPages/AllCats";
import CatProfile from "../src/CatPages/CatProfile";
import AddCatForm from "./CatPages/AddCatForm";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route exact path="/" element={<AllCats />} />
          <Route exact path="/cats/:id" element={<CatProfile />} />
          <Route exact path="/add-cat" element={<AddCatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
