import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";

const CatProfile = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [cats, setCats] = useState([]);
  const [form, setForm] = useState({ display: "none" });
  const [cat, setCat] = useState({ name: "", image: "" });

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:3001/api/cats/${id}`);
      res
        .json()
        .then((res) => setCats(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [id]);

  const handleEdit = (event, cat) => {
    event.preventDefault();
    setForm({ display: "block" });
    setCat(cat);
  };

    const handleChange = (event) => {
      setCat((prevState) => ({
        ...prevState,
        [event.target.name]: event.target.value,
      }));
    };

      const handleSubmit = (event) => {
        event.preventDefault();
        // console.log(id);
        fetch(`/api/cats/${id}`, {
          method: "put",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },

          //make sure to serialize your JSON body
          body: JSON.stringify(cat),
        }).then((response) => response.json());
        navigate("/");
      };

  return (
    <div>
      <h1>Jen's Cat Adoption Cafe Website</h1>
      {cats.map((cat) => (
        <div key={cat.id}>
          <p>{cat.name}</p>
          <img src={cat.image} alt="" width="300" height="300" />
          <button onClick={(e) => handleEdit(e, cat)}>Edit Cat</button>
        </div>
      ))}

      <form onSubmit={(e) => handleSubmit(e)} style={form}>
        <div>
          <label>
            Name:
            <input
              type="text"
              name="name"
              value={cat.name}
              onChange={handleChange}
            />
          </label>
        </div>
        <div>
          <label>
            Photo:
            <input
              type="text"
              name="image"
              value={cat.image}
              onChange={handleChange}
            />
          </label>
        </div>
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
};

export default CatProfile;
