import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const AllCats = (props) => {
  const [cats, setCats] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:3001/api/cats");
      res
        .json()
        .then((res) => setCats(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, []);

  const catProfileRoute = (event, cat) => {
    event.preventDefault();
    console.log(cat);
    let path = `/cats/${cat.id}`;
    navigate(path);
  };

  return (
    <div>
      <h1>Jen's Cat Adoption Cafe Website</h1>
      {cats.map((cat) => (
        <div key={cat.id}>
          <p>{cat.name}</p>
          <img src={cat.image} alt="" width="300" height="300" />
          <button onClick={(e) => catProfileRoute(e, cat)}>
            View cat profile
          </button>
        </div>
      ))}
    </div>
  );
};

export default AllCats;
